Ping Pong Championship
======================

## Installation

1. Install `python-dev` package on your OS.
```bash
sudo apt-get install python-dev
```
2. Pull the latest code from the repository into a directory (Say, `X`).
3. Initialize Python virtualenv in that repository directory.
```bash
virtualenv X
```
4. Activate virtualenv and install the required packges.
```bash
. bin/activate
pip install -r requirements.txt
```
5. Start the referee server.
```bash
cd src/python/ping_pong
python manage.py referee_server
```
6. Open a new terminal tab/window, activate virtualenv and start the client. The client runs on port `8000`.
```bash
. bin/activate
cd src/python/ping_pong
gunicorn ping_pong.wsgi -w 2
```

## Usage

1. All players must first join the game. A player can join the game at `/join` endpoint.
2. Once all players have joined, matchmaking happens. Live match reports are now accessible at `/report` endpoint. A player can view the assigned game ID at `/game_id`.
3. Player can play the turns at `/turn`. If a player wins the game, until all other matches are finished, he/she has to wait for the next round.
4. Once all rounds are over, player can view the next game ID at the same endpoint mentioned before.
5. Player who wins the final round is the winner.

## Endpoints

Endpoint | Method | Parameters | Comments
--- | --- | --- | ---
`/join` | `POST` | `username` - Username. `password` - Password. | Authenticates the user and adds him/her to the matchmaking.
`/game_id` | `GET` | `username` - Username | Fetches the current game ID of the specified user.
`/play_turn` | `POST` | `username` - Username. `game_id` - Game ID. `number` - Number selected by the user. Should be between 1 and 10. | Plays the turn of the specified user.
`/report` | `GET` | None | Generates a live report of the matches.

