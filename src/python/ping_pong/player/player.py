import json
import os

from random import sample

class Player(object):
    @staticmethod
    def create_defence_array(players, player_name):
        defence_length = players[player_name]['defence_length']
        return sample(xrange(1, 11), defence_length)

    @staticmethod
    def read_player_config():
        config = dict()
        path = os.path.join(os.path.dirname(__file__), 'config.json')
        
        with open(path, 'r') as players_conf:
            config = json.load(players_conf)

        return config

    @staticmethod
    def authenticate(players, player_name, password):
        auth = False
        user_id = -1
        defence_length = -1
        
        try:
            if players[player_name]['password'] == password:
                auth = True
                user_id = player.get('user_id', -1)
                defence_length = player.get('defence_length', -1)
        except:
            pass

        return {
            'auth': auth,
            'user_id': user_id,
            'defence_length': defence_length,
        }
