import json

import zerorpc

from django.http import HttpResponse
from zerorpc.exceptions import LostRemote
from zerorpc.exceptions import RemoteError

from referee_rpc import Referee

referee_client = zerorpc.Client()
referee_client.connect('tcp://127.0.0.1:10000')

STATUS_CODE_MAPPING = {
    Referee.StatusCodes.ALL_OK: 200,
    Referee.StatusCodes.ERROR_GAME_NOT_FOUND: 404,
    Referee.StatusCodes.ERROR_CANNOT_JOIN_TOURNAMENT: 403,
    Referee.StatusCodes.ERROR_GAME_EXPIRED: 410,
    Referee.StatusCodes.ERROR_NUMBER_NOT_IN_RANGE: 400,
    Referee.StatusCodes.ERROR_MATCH_ENDED: 406,
    Referee.StatusCodes.ERROR_USER_NOT_IN_MATCH: 400,
    Referee.StatusCodes.ERROR_OPPONENT_TURN: 406,
    Referee.StatusCodes.ERROR_TOURNAMENT_ENDED: 410,
    Referee.StatusCodes.ERROR_TOURNAMENT_NOT_STARTED: 400
}

def response(data, status_code=200):
    response = HttpResponse(json.dumps(data), content_type='application/json')
    response.status_code = status_code
    return response

def response_handler(packet):
    status = packet.get('status')
    message = packet.get('message')
    data = packet.get('data')

    if status is None:
        return response('Internal server error.', 503)

    return response({
        'message': message,
        'data': data,
    }, STATUS_CODE_MAPPING.get(status, 500))

def request_validator(method, required_params=list()):
    def inner(func):
        def wrapper(request):
            if request.method != method:
                return response('Invalid method. Expected %s.' % method, 405)

            if required_params:
                parameters = dict()

                if request.method == 'GET':
                    parameters = request.GET.dict()
                else:
                    parameters = json.loads(request.body)

                if not (set(required_params) <= set(parameters.keys())):
                    return response(
                            'Some mandatory parameters not specified.', 400)
            try:
                return func(request)
            except Exception as ex:
                return response('Exception occured: %s' % ex, 500)

        return wrapper
    return inner

@request_validator('GET')
def get_game_status(request):
    status_code = 200
    status = __rpc('get_game_status')

    if status is None:
        status_code = 503
        status = Referee.StatusCodes.GAME_NOT_STARTED

    return response('Server status: %d.' % (status, ), status_code)

@request_validator('POST', ('username', 'password', ))
def join(request):
    params = json.loads(request.body)

    username = params.get('username')
    password = params.get('password')

    auth_response = __rpc('authenticate', username, password)

    if not auth_response.get('auth', False):
        return response('Username or password invalid.', 403)

    has_joined = __rpc('join', username)
    return response_handler(has_joined)

@request_validator('DELETE', ('username', ))
def leave(request):
    params = json.loads(request.body)

    username = params.get('username')

@request_validator('GET', ('username', ))
def game_id(request):
    params = request.GET.dict()

    username = params.get('username')

    return response_handler(__rpc('get_game_id', username))

@request_validator('POST', ('game_id', 'username', 'number', ))
def play_turn(request):
    params = json.loads(request.body)

    game_id = params.get('game_id')
    username = params.get('username')
    number = params.get('number')

    turn_response = __rpc('play_turn', game_id, username, number)
    return response_handler(turn_response)

@request_validator('GET')
def report(request):
    report_response = __rpc('get_report')
    return response_handler(report_response)

def __rpc(func, *args, **kwargs):
    try:
        return getattr(referee_client, func)(*args, **kwargs)
    except (LostRemote, RemoteError, NameError):
        pass
