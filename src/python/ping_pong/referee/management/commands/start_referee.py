import os
import sys

import zerorpc

from django.core.management.base import BaseCommand, CommandError

PATH = lambda x: os.path.realpath(os.path.join(os.path.abspath(__file__), x))
sys.path.append(PATH('../../../'))

from referee_rpc import Referee

class Command(BaseCommand):
    help = 'Starts the Referee server.'

    def handle(self, *args, **options):
        self.stdout.write('Starting Referee server.')
        server = zerorpc.Server(Referee())
        server.bind('tcp://127.0.0.1:10000')
        server.run()
