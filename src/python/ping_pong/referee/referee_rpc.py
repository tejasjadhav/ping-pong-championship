"""Module for Referee RPC class."""

from copy import deepcopy
from math import ceil
from math import log
from random import randint
from uuid import uuid4

from player.player import Player

class Referee(object):
    """RPC class for the Referee instance. Contains all functionality
    related to the game including round management, game logic, player
    management and report generation."""

    MAX_PLAYERS = 8
    MAX_ROUNDS = int(ceil(log(MAX_PLAYERS, 2)))

    POINTS_PER_SCORE = 1
    MINIMUM_POINTS_TO_WIN = 5

    class StatusCodes(object):
        """Encapsulation for all the status codes used thorughout the
        game."""
        GAME_NOT_STARTED = 0
        GAME_WAITING_FOR_PLAYERS = 1
        GAME_STARTED = 2
        GAME_ENDED = 3

        ROUND_STARTED = 0
        ROUND_ENDED = 1

        MATCH_STARTED = 0
        MATCH_ENDED = 1

        ALL_OK = 0
        ERROR_GAME_NOT_FOUND = 100
        ERROR_CANNOT_JOIN_TOURNAMENT = 101
        ERROR_GAME_EXPIRED = 102
        ERROR_NUMBER_NOT_IN_RANGE = 103
        ERROR_MATCH_ENDED = 104
        ERROR_USER_NOT_IN_MATCH = 105
        ERROR_OPPONENT_TURN = 106
        ERROR_TOURNAMENT_ENDED = 107
        ERROR_TOURNAMENT_NOT_STARTED = 108

    def __init__(self):
        """Creates a Referee instance and initializes the game."""
        self.__current_round = 1
        self.__current_round_players = list()
        self.__game_status = Referee.StatusCodes.GAME_WAITING_FOR_PLAYERS
        self.__player_config = Player.read_player_config()
        self.__rounds = dict()
        self.__game_ids = dict()

        print 'Starting Referee RPC service.'

    @staticmethod
    def response(status, message, data=None):
        """Returns a formatted packet."""
        return {
            'status': status,
            'message': message,
            'data': data,
        }

    def get_game_status(self):
        """Returns the game status for all the current games."""
        return Referee.response(
            Referee.StatusCodes.ALL_OK,
            '',
            self.__game_status)

    def get_report(self):
        """Prepares and returns a detailed report of the tournament."""

        if not self.__rounds:
            return Referee.response(
                Referee.StatusCodes.ERROR_TOURNAMENT_NOT_STARTED,
                'Tournament yet to start.')

        matches = dict()

        for round_number in xrange(1, Referee.MAX_ROUNDS + 1):
            if round_number not in self.__rounds:
                break

            round_name = 'Round %d' % round_number
            matches[round_name] = dict()
            games = list()
            all_matches_finished = True
            all_rounds_ended = False

            for game_id, game_data in self.__rounds[round_number].iteritems():
                match_data = deepcopy(game_data)

                match_data['game_id'] = game_id

                for username in match_data.get('players'):
                    match_data['players'][username].pop('defence')

                if match_data['status'] == Referee.StatusCodes.MATCH_ENDED:
                    if round_number == Referee.MAX_ROUNDS:
                        match_data['status_message'] = 'Round ended.'
                        all_rounds_ended = True
                    else:
                        match_data['status_message'] = 'Match ended.'
                else:
                    match_data['status_message'] = 'Match in progress.'
                    all_matches_finished = False

                games.append(match_data)

            matches[round_name]['games'] = games

            if all_matches_finished:
                if all_rounds_ended:
                    matches[round_name]['status'] = 'Tournament ended.'
                else:
                    matches[round_name]['status'] = 'Round ended.'
            else:
                matches[round_name]['status'] = 'Round in progress.'

        return Referee.response(
            Referee.StatusCodes.ALL_OK,
            'Report generated.',
            matches)

    def get_game_id(self, username):
        """Returns the game ID of an ongoing game the specified user is
        playing.

        Arguments:
        username -- Username.
        """
        if not self.__game_ids.get(username):
            return Referee.response(
                Referee.StatusCodes.ERROR_GAME_NOT_FOUND,
                'Game not found.')

        return Referee.response(
            Referee.StatusCodes.ALL_OK,
            'Game found.', self.__game_ids.get(username))

    def authenticate(self, username, password):
        """Authenticates the specified username against the specified
        password.

        Arguments:
        username -- Username.
        password -- Password.
        """
        players = self.__player_config.get('players', dict())
        return Player.authenticate(players, username, password)

    def join(self, username):
        """Joins the specified user to the initial matchmaking. A user
        can join in the first round only when the game is still waiting
        for other players to join.

        Arguments:
        username -- Username.
        """
        if self.__game_status == Referee.StatusCodes.GAME_ENDED:
            return Referee.response(
                Referee.StatusCodes.ERROR_TOURNAMENT_ENDED,
                'Tournament ended. Check results.')

        if self.__game_status != Referee.StatusCodes.GAME_WAITING_FOR_PLAYERS:
            return Referee.response(
                Referee.StatusCodes.ERROR_CANNOT_JOIN_TOURNAMENT,
                'Cannot join the tournament now.')

        if username in self.__current_round_players:
            return Referee.response(
                Referee.StatusCodes.ALL_OK,
                'You\'ve already joined the tournament. '\
                        'Waiting for other players.')

        self.__current_round_players.append(username)

        if len(self.__current_round_players) >= Referee.MAX_PLAYERS:
            self.__game_status = Referee.StatusCodes.GAME_STARTED
            self.__prepare_games()
            return Referee.response(
                Referee.StatusCodes.ALL_OK,
                'You\'ve joined the tournament. Tournament will start now.')

        return Referee.response(
            Referee.StatusCodes.ALL_OK,
            'You\'ve joined the tournament. Waiting for other players.')

    def play_turn(self, game_id, username, number):
        """Plays a turn for the user with the selected number for the
        specified game.

        Arguments:
        game_id -- Game ID. Should be active for the current round.
        username -- Username. User should be the part of the given game ID.
        number -- Number of the offence. Should be between 1 and 10.
        """
        if self.__game_status == Referee.StatusCodes.GAME_ENDED:
            return Referee.response(
                Referee.StatusCodes.ERROR_TOURNAMENT_ENDED,
                'Tournament ended. Check results.')

        if game_id not in self.__rounds[self.__current_round]:
            return Referee.response(
                Referee.StatusCodes.ERROR_GAME_EXPIRED,
                'Game ID specified has expired.')

        if  number > 10 or number < 1:
            return Referee.response(
                Referee.StatusCodes.ERROR_NUMBER_NOT_IN_RANGE,
                'Number should be between 1 and 10.')

        match = self.__rounds[self.__current_round][game_id]
        players = deepcopy(match.get('players'))

        if match['status'] == Referee.StatusCodes.MATCH_ENDED:
            return Referee.response(
                Referee.StatusCodes.ERROR_MATCH_ENDED,
                'Match already ended.')

        if username not in players:
            return Referee.response(
                Referee.StatusCodes.ERROR_USER_NOT_IN_MATCH,
                'User not a part of the game.')

        if match['current_turn'] != username:
            return Referee.response(
                Referee.StatusCodes.ERROR_OPPONENT_TURN,
                'Opponent\'s turn now.')

        print match

        players.pop(username)
        opp_username, opp_data = players.popitem()

        point_scored_by = username

        if number in opp_data['defence']:
            point_scored_by = opp_username
            match['current_turn'] = opp_username

        print point_scored_by

        is_winner = Referee.__check_and_set_winner(match, point_scored_by)
        self.__rounds[self.__current_round][game_id] = match

        message = None

        if is_winner:
            self.__rounds[self.__current_round][game_id]['current_turn'] = None
            self.__game_ids[username] = None
            self.__game_ids[opp_username] = None

            current_match_count = 2 ** \
                (Referee.MAX_ROUNDS - self.__current_round)
            finished_match_count = 0
            winners = list()

            for game_id, game_data in \
                    self.__rounds[self.__current_round].iteritems():
                if game_data['status'] == Referee.StatusCodes.MATCH_ENDED:
                    finished_match_count += 1
                    winners.append(game_data['winner'])

            if finished_match_count >= current_match_count:
                self.__current_round += 1

                if self.__current_round > Referee.MAX_ROUNDS:
                    self.__game_status = Referee.StatusCodes.GAME_ENDED
                    self.__current_round_players = list()

                    if point_scored_by == username:
                        message = 'Congratulations! You won the tournament.'
                    else:
                        message = 'Sorry, you lost the tournament.'
                else:
                    self.__current_round_players = winners
                    self.__prepare_games()

                    if point_scored_by == username:
                        message = 'Congratulations! You won the round.'
                    else:
                        message = 'Sorry, but you lost the round.'
            else:
                if point_scored_by == username:
                    message = 'Congratulations! You won the round. ' \
                            'Wait till the other rounds get over.'
                else:
                    message = 'Sorry, but you lost the round.'
        else:
            if point_scored_by == username:
                message = 'You scored a point. Your turn again.'
            else:
                message = 'Your offence was blocked. ' \
                        'Opponent scored a point. Opponent\'s turn now.'

        return Referee.response(Referee.StatusCodes.ALL_OK, message)

    @staticmethod
    def __check_and_set_winner(match, username):
        """Checks whether the current turn makes the user a winner.
        If yes, then set the user as winner and finish the match."""
        match['players'][username]['score'] += Referee.POINTS_PER_SCORE

        if match['players'][username]['score'] >= Referee.MINIMUM_POINTS_TO_WIN:
            match['status'] = Referee.StatusCodes.MATCH_ENDED
            match['winner'] = username
            return True

        return False

    def __prepare_games(self):
        """Prepares the game for the current round with existing players
        based on a random matchmaking."""
        current_players = self.__current_round_players
        player_count = len(current_players)

        if not log(player_count, 2).is_integer():
            raise Exception('Invalid player count.')

        self.__rounds[self.__current_round] = dict()
        random_integer = lambda: randint(0, len(current_players) - 1)

        while current_players:
            player_1 = current_players.pop(random_integer())
            player_2 = current_players.pop(random_integer())

            game_id = str(uuid4())

            self.__game_ids[player_1] = game_id
            self.__game_ids[player_2] = game_id

            players = self.__player_config.get('players', dict())

            self.__rounds[self.__current_round][game_id] = {
                'players': {
                    player_1: {
                        'score': 0,
                        'defence': Player.create_defence_array(
                            players, player_1),
                    },
                    player_2: {
                        'score': 0,
                        'defence': Player.create_defence_array(
                            players, player_2),
                    },
                },
                'current_turn': player_1,
                'status': Referee.StatusCodes.MATCH_STARTED,
                'winner': None,
            }
